package edu.bu.ec504.spr24.sameGameTris;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;
import java.io.Serial;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.ButtonGroup;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import edu.bu.ec504.spr24.brain.SmarterBrain;
import edu.bu.ec504.spr24.highScores.highScore;
import edu.bu.ec504.spr24.brain.Brain;
import edu.bu.ec504.spr24.brain.LazyBrain;
import edu.bu.ec504.spr24.brain.SimpleBrain;

import static java.util.concurrent.Executors.newScheduledThreadPool;

/**
 * A Singleton Graphical User Interface for the game.
 * @author Ari Trachtenberg
 */
public class SameGameTris extends GUI implements ActionListener, ItemListener {

  // CONSTANTS
  static public final int DEFAULT_WIDTH = 15, DEFAULT_HEIGHT = 10; // sizes in terms of numbers of circles
  /**
   * Rate at which new "Tetrising" circles are being produced, in microseconds.
   * Production rate increases invesely proportional to the number of circle selections clicked so far.
   */
  static private long PRODUCT_CIRCLE_US = 10000000;
  /**
   * Rate at which new "Tetrising" circles descend on the board, in microseconds.
   * Production rate increases invesely proportional to the number of circle selections clicked so far.
   */
  static private long DROP_CIRCLE_US = 5000000;

  /**
   * The number of microseconds between Brain moves
   */
  private static final long BRAIN_WAIT_US = 1000000;

  /**
   * The amount of time to wait between games, to allow for existing elements to settle.
   */
  private static final long TIME_BETWEEN_GAMES_MS = 1000;

  /**
   * The number of empty rows on the top of the game.
   */
  static public final int DEFAULT_EMPTY_ROWS = DEFAULT_HEIGHT / 3;

  /**
   * The default window size, if run as a standalone application.
   */
  static public final int DEFAULT_WINDOW_WIDTH = 500, DEFAULT_WINDOW_HEIGHT = 300;

  /**
   * The number of colors available for circles.
   */
  static public final int NUM_COLORS = 3;

  /**
   * The number of milliseconds to wait after selecting a region before disappearing it.
   * This allows the user to see the region being selected
   */
  static private final int DISPLAY_WAIT_MS = 700;

  /**
   * The file where high scores are stored.
   */
  static private final String HIGH_SCORE_FILE = ".highscores.db";

  @Serial
  static private final long serialVersionUID = 1L;

  /**
   * Keeps track of the number of points for the selected region.
   */
  final JLabel regionPoints = new JLabel("0");

  /**
   * Keeps track of the total number of points so far
   */
  final JLabel totalPoints = new JLabel("0");

  // FIELDS
  private int width = DEFAULT_WIDTH, height = DEFAULT_HEIGHT; // initial width and height of the array of circles
  private final int emptyRows = DEFAULT_EMPTY_ROWS;         // number of rows on top of screen with no circles
  private SelfAwareCircle[][] circles;
  private String highScoreName = null;              // the name to use for the high score
  static final Random randGen = new Random();       // package-private random number generator used for the entire package (i.e., repeatedly seeding with the same value produces the same randomness).
  /**
   * Counts the number of clicks on circles since the last time the board was reset.
   * @requires must be a positive integer; c
   */
  static private int numClicks = 1;

  /**
   * true iff the game is over (i.e., not running)
   */
  boolean gameOver = false;
  /**
   * An instance of the SGE GUI.
   */
  private static SameGameTris Instance = null;

  // ... GUI elements
  private JPanel circlePanel; // the panel containing the circles
  private JPanel textPanel;   // the panel containing scoring information
  // ... ... menu items
  private JMenuItem changeBoard;
  private JMenuItem quitGame;
  private JRadioButtonMenuItem noPlayerMenuItem, simplePlayerMenuItem, lazyPlayerMenuItem, // various automatic player options
      smarterPlayerMenuItem;
  private final ReentrantLock GUIlock = new ReentrantLock(); // thread lock used to synchronize manipulation of circles on the board

  // ... Brain elements
  Brain theBrain;                   // the Brain (automated player) for the game
  Thread brainThread;               // the thread that will run the Brain
  final ScheduledExecutorService brainExec = newScheduledThreadPool(2); // set up the brain threads
  private ScheduledFuture<?> brainFuture = null; // schedule for Brain events (i.e., make moves)

  // ... Tetris executor
  final ScheduledExecutorService tetrisExec = newScheduledThreadPool(2); // sets up tetris threads
  private ScheduledFuture<?> produceCircleFuture = null; // schedule for producing circles for the app
  private ScheduledFuture<?> dropCircleFuture = null;    // schedule for dropping circles for the app


  // SUBCLASSES

  /**
   * Drops a new circle at the top of the board every time it is run.
   * If this is not possible, then the gameOverSignal is set to true
   */
  private class initDropCircles implements Runnable {

    @Override
    public void run() {
      GUIlock.lock(); // lock circle manipulations down

      try {
        int randColumn = randGen.nextInt(width);
        SelfAwareCircle myCircle = circles[randColumn][0];
        if (myCircle.get_state()
            != SelfAwareCircle.Visibility.clear) { // i.e. the spot is already occupied
          doGameOver("overran column " + randColumn);
        } else {
          myCircle.resetColor();
          myCircle.set_state(SelfAwareCircle.Visibility.plain);
        }
      } finally {
        GUIlock.unlock();
      }

      repaint();
    }
  }

  /**
   * Moves circles down until they are connected to the main collection of circles.
   */
  private class moveDropCircles implements Runnable {

    @Override
    public void run() {
      GUIlock.lock(); // lock circle manipulations down

      try {
        // hunt for a circle that is above a clear circle
        for (int xx = 0; xx < width; xx++)
          for (int yy = height - 2; yy >= 0;
              yy--) {  // go down to up, so as not to move circles more than once
            SelfAwareCircle orig = circles[xx][yy],
                dest = circles[xx][yy + 1];
            if (orig.get_state() != SelfAwareCircle.Visibility.clear &&        // a colored circle
                dest.get_state() == SelfAwareCircle.Visibility.clear) {    // above a cleared circle
              // move it down
              moveCircle(orig, dest);
            }
          }
      } finally {
        GUIlock.unlock();
      }
      repaint();
    }
  }

  // METHODS

  /*
   * Default no-args constructor
   */
  private SameGameTris() {
    super("Ari's samegame");
  }

  /**
   * Fire up the GUI.
   */
  private void run() {
    try {
      SwingUtilities.invokeAndWait(
          this::setupGUI
      );
    } catch (Exception e) {
      System.out.println("Saw exception " + e);
    }
  }

  public static SameGameTris getInstance() {
    if (SameGameTris.Instance == null) {
      // no Instance has been set up yet ... make it happen.
      SameGameTris.Instance = new SameGameTris();
      SameGameTris.Instance.run();
    }
    return Instance;
  }

  /*
   * Returns the color of the circle at location [xx][yy], or NONE if the circle has been cleared
   * @param xx must be between 0 and width
   * @param yy must be between 0 and height
   */
  public CircleColor colorAt(int xx, int yy) {
    if (circles[xx][yy].isCleared())
      return CircleColor.NONE;
    else
      return circles[xx][yy].clr;
  }

  /*
   * Returns the width of the current board
   */
  public int boardWidth() {
    return width;
  }

  /*
   * Returns the height of the current board
   */
  public int boardHeight() {
    return height;
  }

  /*
   * Returns true iff the game is over
   * (i.e. every circle is surrounded by cleared circles or circles of a different color)
   */
  public boolean gameOverQ() {
    if (gameOver)
      return true;

    for (int xx = 0; xx < width; xx++)
      for (int yy = 0; yy < height; yy++) {
        if (!circles[xx][yy].isCleared()) {
          CircleColor myColor = circles[xx][yy].getColor();
          // check its neighbors
          if (sameColor(myColor, xx - 1, yy) ||
              sameColor(myColor, xx + 1, yy) ||
              sameColor(myColor, xx, yy - 1) ||
              sameColor(myColor, xx, yy + 1))
            return false; // the game has not ended
        }
      }

    return true; // there are no viable moves
  }

  /*
   * "Clicks" on the circle at location (xx,yy)
   */
  @Override
  public void makeMove(int xx, int yy) {
    circles[xx][yy].mouseEntered(null);  // pretend the mouse was pressed at location (xx,yy)
    Timer t = new Timer();
    TimerTask tt = new TimerTask() {
      @Override
      public void run() {
        final SelfAwareCircle theCircle = circles[xx][yy];
        theCircle.mouseExited(null);
        theCircle.mousePressed(null);
        theCircle.mouseReleased(
            null); // pretend that the mouse button was released at the location
      }
    };
    t.schedule(tt, DISPLAY_WAIT_MS);
  }

  /**
   * @return the score achieved from clicking on a region of length <b>level</b>
   */

  @Override
  final public int score(int level, CircleColor clr) {
    if (level == 1)
      return 1;
    else
      return level * level;
  }

  // OTHER METHODS

  /*
   * setupGUI helper
   *   - adds to circle[ii][jj] a SelfAwareListener of circle[xx][yy] as 0<=xx<width and 0<=yy<height
   * @param ii = x coordinate of the circle to whom we will be listening
   * @param jj = y coordinate of the circle to whom we will be listening
   * @param xx = x coordinate of the listener circle
   * @param yy = y coordinate of the listener circle
   */

  private void addNeighbor(int ii, int jj, int xx, int yy) {
    if ((0 <= xx && xx < width) &&
        (0 <= yy && yy < height))
      circles[ii][jj].addSelfAwareListener(circles[xx][yy]);
  }

  /**
   * Set up the initial screen items
   * NOTE:  must perform cleanUp() first if there is already a GUI set up on the screen
   */
  void setupGUI() {

    // SET LAYOUT STYLE
    setLayout(new BorderLayout());

    // SET UP MENUS
    JMenuBar menuBar = new JMenuBar();

    // ... File Menu
    JMenu menu = new JMenu("File");

    changeBoard = new JMenuItem("Change board");
    changeBoard.addActionListener(this);
    menu.add(changeBoard);

    quitGame = new JMenuItem("Quit");
    quitGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.META_DOWN_MASK));
    quitGame.setMnemonic('Q');
    quitGame.addActionListener(this);
    menu.add(quitGame);

    menuBar.add(menu);

    // ... Automatic play Menu
    menu = new JMenu("Automatic play");
    ButtonGroup group = new ButtonGroup();

    noPlayerMenuItem = new JRadioButtonMenuItem("none");
    noPlayerMenuItem.addItemListener(this);
    noPlayerMenuItem.setSelected(true); // i.e. the default item
    group.add(noPlayerMenuItem);
    menu.add(noPlayerMenuItem);
    menu.addSeparator();

    simplePlayerMenuItem = new JRadioButtonMenuItem("Simple player");
    simplePlayerMenuItem.addItemListener(this);
    group.add(simplePlayerMenuItem);
    menu.add(simplePlayerMenuItem);

    lazyPlayerMenuItem = new JRadioButtonMenuItem("Lazy player");
    lazyPlayerMenuItem.addItemListener(this);
    group.add(lazyPlayerMenuItem);
    menu.add(lazyPlayerMenuItem);

    smarterPlayerMenuItem = new JRadioButtonMenuItem("Smarter player");
    smarterPlayerMenuItem.addItemListener(this);
    group.add(smarterPlayerMenuItem);
    menu.add(smarterPlayerMenuItem);
    menuBar.add(menu);

    this.setJMenuBar(menuBar);

    // SET UP CIRCLE PANEL
    circlePanel = new JPanel(new GridLayout(height, width));
    // ... allocate circles
    circles = new SelfAwareCircle[width][height];

    // ..... set up some circles
    for (int jj = 0; jj < height; jj++)
      for (int ii = 0; ii < width; ii++) {
        // establish the button
        if (jj <= emptyRows) {
          circles[ii][jj] = new SelfAwareCircle(CircleColor.NONE, ii,
              jj); // the top emptyRows should have a circle with no color
        }
        else {
          circles[ii][jj] = new SelfAwareCircle(ii, jj);
        }
        circlePanel.add(circles[ii][jj]);
      }
    // ... set up listeners in the area
    for (int xx = 0; xx < width; xx++)
      for (int yy = 0; yy < height; yy++) {
        // add all neighbors
        addNeighbor(xx, yy, xx, yy - 1);
        addNeighbor(xx, yy, xx, yy + 1);
        addNeighbor(xx, yy, xx - 1, yy);
        addNeighbor(xx, yy, xx + 1, yy);
      }

    add(circlePanel, BorderLayout.CENTER);

    // ... set up the text panel
    textPanel = new JPanel(new FlowLayout());
    textPanel.add(new JLabel("Selected len: "));
    textPanel.add(regionPoints);
    textPanel.add(new JLabel("")); // i.e. blank
    textPanel.add(new JLabel("Total pts: "));
    textPanel.add(totalPoints);
    add(textPanel, BorderLayout.SOUTH);

    // paint the display
    validate();
    repaint();
  }

  /**
   * Increment the number of clicks seen.
   * If this ends up being greater than the size of the board, the game is declared over.
   */
  public void updateNumClicks() {
    numClicks++;
    if (numClicks > width * height)
      doGameOver("too many clicks - " + numClicks);
  }


  /**
   * Cancel schedules for adding / dropping circles.
   */
  private void cancelTetrisingSchedules() {
    if (produceCircleFuture != null)
      produceCircleFuture.cancel(true);
    if (dropCircleFuture != null)
      dropCircleFuture.cancel(true);
  }

  /**
   * @return true iff tetrising is going on
   */
  public boolean isTetrisingQ() {
    return produceCircleFuture!=null && produceCircleFuture.isCancelled()==false;
  }
  /**
   * Sets up threads that drop circles onto the board and move them down over time.
   * As {@link #numClicks} gets larger, circles come more often and drop faster.
   */
  public void restartTetrising() {
    // cancel the old schedules
    cancelTetrisingSchedules();

    // start up the new ones
    produceCircleFuture = tetrisExec.scheduleAtFixedRate(new initDropCircles(), 0,
        PRODUCT_CIRCLE_US / numClicks, TimeUnit.MICROSECONDS);
    dropCircleFuture = tetrisExec.scheduleAtFixedRate(new moveDropCircles(), 0, DROP_CIRCLE_US / numClicks,
        TimeUnit.MICROSECONDS);
  }

  /*
   * Performs any clean up actions needed before setting up a new GUI
   */
  void cleanUp() {
    // Removes elements for garbage collection

    // close down the brain
    highScoreName = null;
    stopBrain();

    // score panel
    totalPoints.setText("0");
    textPanel.setVisible(false);
    textPanel = null;

    // circles
    for (int ii = 0; ii < width; ii++)
      for (int jj = 0; jj < height; jj++) {
        circles[ii][jj].removeMouseListener(circles[ii][jj]);
        circles[ii][jj] = null;
      }

    // circle panel
    circlePanel.setVisible(false);
    circlePanel = null;

    // cancel tetrising schedules
    numClicks = 1;  // reset the number of circle clicks in the system
    cancelTetrisingSchedules();

    // reset gameOver
    gameOver = false;
  }

  /*
   * Returns true iff circles[xx][yy] has color theColor, is not cleared, AND (xx,yy) is within the range of the board
   */
  private boolean sameColor(CircleColor theColor, int xx, int yy) {
    if (xx < 0 || yy < 0 || xx >= width || yy >= height || circles[xx][yy].isCleared())
      return false;
    else
      return circles[xx][yy].getColor().equals(theColor);
  }

  /*
   * Moves circle c1 into location c2, leaving c1 as a clear circle that
   * does not receive mouse events
   */
  synchronized private void moveCircle(SelfAwareCircle orig, SelfAwareCircle dest) {
    // copy the immutable, position independent values
    orig.copyTo(dest);

    // clear the top item
    orig.setClear();
  }

  /*
   * Called to request a reshifting of the board (as necessary).
   *    This should happen if some circles are rendered "clear"ed
   */

  final void shiftCircles() {
    /* start at the bottom and move up ... all cleared circles are
     *    removed, with upper circles falling into their positions;
     *    if a column is totally empty, then its rightmost columns
     *    shift into it
     */

    GUIlock.lock(); // acquire a lock

    try {
//      // 1.  SHIFT VERTICALLY
//      for (int xx = 0; xx < width; xx++) {
//        int firstClr = height - 1;  // the lowest cleared entry in the column
//        int firstFull =
//            height - 1; // the lowest uncleared entry in the column that has not yet been processed
//        boolean moveOn = false;      // set to true in order to move on to the next column
//        while (!moveOn) {
//          // find the lowest clear entry in the column (if it exists)
//          try {
//            while (!circles[xx][firstClr].isCleared())
//              firstClr--;
//          } catch (ArrayIndexOutOfBoundsException e) {
//            moveOn = true;
//            continue;  // i.e. no cleared circle found in this column --- go to the next column
//          }
//
//          if (firstFull > firstClr)
//            firstFull = firstClr; // only move items "down" the column
//
//          // find the lowest non-cleared entry in the column (if it exists)
//          try {
//            while (circles[xx][firstFull].isCleared())
//              firstFull--;
//          } catch (ArrayIndexOutOfBoundsException e) {
//            moveOn = true;
//            continue;  // i.e. the whole column is clear --- for now, go to the next column
//          }
//
//          moveCircle(circles[xx][firstFull], circles[xx][firstClr]);
//
//          firstFull--;
//          firstClr--; // iterate
//        }
//      }

      // 2.  SHIFT HORIZONTALLY
      // Check to see if any column is now empty
      // ... this could have been done within the loop above, but it would detract from readability of the code
      boolean emptySoFar = true;       // remains true if all columns seen so far have only cleared circles
      for (int xx = width - 1; xx >= 0; xx--) {
        boolean allCleared = true;   // remains true if all circles in column xx have been cleared
        for (int yy = 0; yy < height; yy++)
          if (!circles[xx][yy].isCleared()) {
            allCleared = false;
            break;
          }

        if (allCleared) {
          if (!emptySoFar) { // i.e. do not do anything with empty columns on the right of the screen
            // move other columns into this empty column
            for (int ii = xx + 1; ii < width; ii++)
              for (int jj = 0; jj < height; jj++)
                moveCircle(circles[ii][jj], circles[ii - 1][jj]);
          }
        } else
          emptySoFar = false;
      }

      // 3.  (LAST ITEM) CHECK IF THE GAME HAS ENDED
      // This happens if every circle is surrounded by cleared circles or circles of a different color
      if (gameOverQ()) {
        doGameOver("exhausted all circles"); // all done
      }
    } finally {
      GUIlock.unlock(); // release the circles for other processes
    }
  }

  /**
   * The game is over - report to the user and/or save a high score, if relevant.
   *
   * @param feedback Some string to present to the user; typically the reason that the game is over.
   */
  private void doGameOver(String feedback) {
    // close out other threads and variables
    gameOver = true;
    cancelTetrisingSchedules(); // cancel Tetrising threads
    stopBrain();                // stop the thinking Brain

    // check the high score
    int score = Integer.parseInt(totalPoints.getText());
    highScore hs = new highScore(HIGH_SCORE_FILE);

    if (hs.newRecordQ(score)) { // i.e. a new record
      _showMessage(null,
          "Game Over (" + feedback + ") - You got a new high score of " + score + " points!\n" +
              "Your weighted score for this sized board was " + highScore.hsComp.weightedScore(
              new highScore.HSdata("", width, height, score)) + ".");
      if (highScoreName == null)
        highScoreName = _showInput(null,
            "You got a new high score!\nPlease enter your name:");

      if (highScoreName != null) { // i.e. the user is interested in high scores
        // populate the high score item
        highScore.HSdata datum = new highScore.HSdata(highScoreName, width, height, score);
        hs.putScore(datum); // enter the name into the high score list
      }
    } else {
      _showMessage(null,
          "Game Over (" + feedback + ") - You did not make the high score.  You had " + score
              + " points.\n" +
              "Your weighted score for this sized board was " + highScore.hsComp.weightedScore(
              new highScore.HSdata("", width, height, score)) + ".");
    }
    _showMessage(null, "Current high scores:\n" + hs.display());

//    // cleanup
//    cleanUp();
//    setupGUI();

    // "that's all folks"
    System.exit(0);
  }

  /**
   * Do a task in the future.
   * @param task The task to complete.
   * @param whenMS How many milliseconds in the future to do the task.
   */
  private void _doInFuture(TimerTask task, long whenMS) {
    Timer timer = new Timer();
    timer.schedule(task, whenMS);
  }

  /**
   * Shows a message on the parent component and waits for the user to click "OK".
   * @param parent The component on which to show the dialog box.
   * @param message The message to display.
   */
  private void _showMessage(Component parent, String message) {
   JOptionPane.showMessageDialog(parent, message);
  }

  /**
   * Shows an input request on the parent component and returns the user's input.
   * @param parent The component on which to show the dialog box.
   * @param message The input request
   */
  private String _showInput(Component parent, String message) {
    return JOptionPane.showInputDialog(parent, message);
  }

  /**
   * Handle menu events
   * @param e The event to handle.
   */
  public void actionPerformed(ActionEvent e) {
    Object source = e.getSource();

    if (source == quitGame)
      System.exit(0);
    else if (source == changeBoard) {
      // modified from http://www.vbforums.com/showthread.php?t=513699
      JTextField width = new JTextField();
      width.setText("" + DEFAULT_WIDTH);
      JTextField height = new JTextField();
      height.setText("" + DEFAULT_HEIGHT);
      Object[] msg = {"Width:", width, "Height:", height};

      JOptionPane op = new JOptionPane(
          msg,
          JOptionPane.QUESTION_MESSAGE,
          JOptionPane.OK_CANCEL_OPTION,
          null,
          null);

      JDialog dialog = op.createDialog(this, "Enter new board size ...");
      dialog.setVisible(true);

      int result = JOptionPane.OK_OPTION;

      try {
        result = (Integer) op.getValue();
      } catch (Exception ignored) {
      }

      if (result == JOptionPane.OK_OPTION) // i.e. effect the change
      {
        // i.e. destroy the old board
        cleanUp();
        this.width = Integer.parseInt(width.getText());
        this.height = Integer.parseInt(height.getText());

        // create the new board
        setupGUI();
      }
    }
  }

  /**
   * Starts a Brain that makes moves
   */
  private void startBrain(Brain theBrain) {
    this.theBrain = theBrain;
    highScoreName = theBrain.myName(); // the computer gets credit for any subsequent high score
//    brainThread = new Thread(theBrain, "Brain Thread");
//    brainThread.start();
    brainFuture = brainExec.scheduleAtFixedRate(theBrain, 0, BRAIN_WAIT_US, TimeUnit.MICROSECONDS);
  }

  /**
   * Stops the Brain that makes move.
   */
  private void stopBrain() {
    if (theBrain != null) {
      theBrain.allDone();
      brainFuture.cancel(true);
    }
  }

  /**
   *   Handles the menu checkBox
    */
  public void itemStateChanged(ItemEvent e) {
    Object source = e.getItemSelectable();

    if (e.getStateChange() == ItemEvent.SELECTED) {
      if (source == noPlayerMenuItem)
        return; // i.e. no players
      else if (source == simplePlayerMenuItem)
        startBrain(new SimpleBrain());
      else if (source == lazyPlayerMenuItem)
        startBrain(new LazyBrain());
      else if (source == smarterPlayerMenuItem)
        startBrain(new SmarterBrain());
    } else {
      // deselected
      if (theBrain != null)
        theBrain.allDone(); // will ask the Brain to stop thinking
    }
  }

  // MAIN METHOD

  /**
   * Runs the application
   */
  public static void main(String[] args) {
    JFrame myApp = SameGameTris.getInstance();
    myApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    myApp.setSize(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT);
    myApp.setVisible(true);
  }
}
