package edu.bu.ec504.spr24.brain;

import edu.bu.ec504.spr24.brain.Board.Pos;

/**
 * A very simple Brain for the game.
 */
public class SimpleBrain extends Brain {

	public SimpleBrain() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	public String myName() {
		return "Simple Brain";
	}

	@Override
	Pos nextMove() {
		return new Pos(0, myGUI.boardHeight() - 1);
	}
}
